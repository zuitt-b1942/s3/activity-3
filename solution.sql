--[USERS]
	INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

--[POST]
	INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (3, "First Code", "Hello Wold", "2021-01-02 01:00:00");
	INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (3, "Second Code", "Hello Earthlings", "2021-01-02 02:00:00");
	INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Third Code", "Welcome to Blackhole!", "2021-01-02 03:00:00");
	INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (6, "Fourth Code", "Bye bye Solar System", "2021-01-02 04:00:00");

--Get all the post with the user id of 3.
	SELECT title FROM posts WHERE user_id = 3;

--Get all user's email and datetime of creation.
	SELECT email, datetime_created FROM users;

--Update "Hello Earthlings" to "Hello Hoomans" by using the record's id.
	UPDATE posts SET content = "Hello Hoomans" WHERE id = 4;

--Delete the user with an email of johndoe@gmail.com.
	DELETE FROM users WHERE email = "johndoe@gmail.com"

